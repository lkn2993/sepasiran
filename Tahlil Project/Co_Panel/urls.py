from django.conf.urls import url

urlpatterns = [
    url(r'^$', 'Co_Panel.views.Co_Panel', name='Co_Panel'),


    url(r'^finance/$', 'Co_Panel.views.finance', name='finance'),
    url(r'^visitors/$', 'Co_Panel.views.visitors', name='visitors'),
    url(r'^popularity/$', 'Co_Panel.views.popularity', name='popularity'),
    url(r'^search/$', 'Co_Panel.views.search', name='search'),
    url(r'^addproduct/$', 'Co_Panel.views.addproduct', name='addproduct'),



    # url(r'^search-result/$', 'app.views.search_result', name='search_result'),
]
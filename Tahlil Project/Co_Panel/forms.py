﻿"""
Definition of forms.
"""
from django import forms
from django.forms.models import inlineformset_factory
from toursNservices.models import Place, Tour, Transportation, Attraction, Sojourn, Food
class TourForm(forms.ModelForm):
    class Meta:
        model = Tour
        fields = ['name', 'description', 'price', 'discount', 'can_reserve', 'start_date', 'end_date',
                  'origin_country', 'origin_city', 'origin_address', 'country', 'city', 'address', 'adult_capacity', 'child_capacity', 'category']
        widgets = {
            'category' : forms.CheckboxSelectMultiple()
            }
        labels = {
            'country' : 'کشور مقصد',
            'city' : 'شهر مقصد',
            'address' : 'آدرس مقصد'
            }
    def __init__(self, *args, **kwargs):
        super(TourForm, self).__init__(*args, **kwargs)
        for field in self: 
            field.field.widget.attrs['class'] = 'form-control' 

TransportationForm = inlineformset_factory(Tour, Transportation, fk_name='related_tour', fields=['name',
                                                                             'description', 'price', 'price_type', 'discount', 'can_reserve', 'start_date', 'end_date',
                                                                             'origin_country', 'origin_city', 'origin_address', 'country', 'city', 'address', 'adult_capacity', 
                                                                             'child_capacity', 'type', 'ticket_number'], labels = {'country' : 'کشور مقصد',
                                                                                                                                   'city' : 'شهر مقصد',
                                                                                                                                   'address' : 'آدرس مقصد'
                                                                                                                                   }, extra=1)
SojournForm = inlineformset_factory(Tour, Sojourn, fk_name='related_tour', fields=['name',
                                                                             'description', 'price', 'discount', 'can_reserve', 'start_date', 'end_date',
                                                                             'country', 'city', 'address', 'adult_capacity', 
                                                                             'child_capacity', 'bed', 'parking'], extra=1)
FoodForm = inlineformset_factory(Tour, Food, fk_name='related_tour', fields=['name',
                                                                             'description', 'price', 'discount', 'can_reserve', 'start_date',
                                                                             'food_services', 'country', 'city', 'address', 'adult_capacity', 
                                                                             'child_capacity'], extra=1)
AttForm = inlineformset_factory(Tour, Attraction, fk_name='related_tour', fields=['name',
                                                                             'description', 'price', 'discount', 'can_reserve', 'start_date', 'end_date',
                                                                             'country', 'city', 'address', 'adult_capacity', 
                                                                             'child_capacity'], extra=1)
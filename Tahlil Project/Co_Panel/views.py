﻿"""
Definition of views.
"""
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.shortcuts import render
from django.http import HttpRequest
from django.template import RequestContext
from datetime import datetime
from Profile.tests import NormalUser
from django.contrib.auth.decorators import login_required, user_passes_test
from toursNservices.models import Place, Service, Tour
from .forms import TourForm, TransportationForm, AttForm, SojournForm, FoodForm
from Profile.forms import LocationForm
@login_required
@user_passes_test(NormalUser)
def Co_Panel(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    return render(request, 'Co_Panel/CW_Home.html', context_instance=RequestContext(request, {})
    )



@login_required
@user_passes_test(NormalUser)
def visitors(request):
    """
    :param request: clicked on search
    :return: search_result page
    """
    assert isinstance(request, HttpRequest)
    return render(request, 'Co_Panel/CW_Visitors.html', context_instance=RequestContext(request, {}))


@login_required
@user_passes_test(NormalUser)
def search(request):
    """
    :param request: clicked on search
    :return: search_result page
    """
    assert isinstance(request, HttpRequest)
    return render(request, 'Co_Panel/CW_Search.html', context_instance=RequestContext(request, {}))


@login_required
@user_passes_test(NormalUser)
def popularity(request):
    """
    :param request: clicked on search
    :return: search_result page
    """
    assert isinstance(request, HttpRequest)
    return render(request, 'Co_Panel/CW_Popularity.html', context_instance=RequestContext(request, {}))


@login_required
@user_passes_test(NormalUser)
def finance(request):
    """
    :param request: clicked on search
    :return: search_result page
    """
    assert isinstance(request, HttpRequest)
    return render(request, 'Co_Panel/CW_Finance.html', context_instance=RequestContext(request, {}))


@login_required
@user_passes_test(NormalUser)
def addproduct(request):
    """
    :param request: clicked on search
    :return: search_result page
    """
    assert isinstance(request, HttpRequest) 
    tour = Tour()
    transportation_formset = TransportationForm(instance=tour, prefix='transportation')
    sojourn_formset = SojournForm(instance=tour, prefix='sojourn')
    food_formset = FoodForm(instance=tour, prefix='food')
    att_formset = AttForm(instance=tour, prefix='att')
    tour_form = TourForm(instance=tour, prefix='tour')
    return render(request, 'Co_Panel/CW_Add_Product.html', {'transportation_formset' : transportation_formset, 'sojourn_formset' : sojourn_formset,
                                                            'food_formset' : food_formset, 'att_formset' : att_formset, 'tour_form' : tour_form})

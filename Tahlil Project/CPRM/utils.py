from django.db.models.query_utils import Q
from django.shortcuts import render
from Profile.models import SepasUser, Add_Info, Ticket
# Create your views here.
from .models import Report
from payment.models import Factor
from toursNservices.models import Comments, Tour, Service
import httplib2
import urllib
import simplejson

def report(field, partner_id=None):
    """
    :return:
    """
    if field == 'users':
        return SepasUser.objects.all()
    if field == 'comments':
        return Comments.objects.all()
    if field == 'tours':
        if partner_id==None:
            return Tour.objects.all()
        else:
            return Tour.objects.filter(provider_id=partner_id)
    if field == 'services':
        if partner_id==None:
            return Service.objects.all()
        else:
            return Service.objects.filter(provider_id=partner_id)
    if field == 'totalpartners':
        return SepasUser.objects.filter(is_staff_member=1)
    if field == 'onlystaff':
        return SepasUser.objects.filter(is_staff_member=1,staff_info__is_agency=False)
    if field == 'agency':
        return SepasUser.objects.filter(is_staff_member=1, staff_info__is_agency=True)
    if field == 'totalfactors':
        if partner_id==None:
             return Factor.objects.all()
        else:
            return Factor.objects.filter(Q(tour__provider_id=partner_id) | Q(service__provider_id=partner_id))
    if field == 'toursales':
         return Factor.objects.filter(tour__provider_id=partner_id)
    if field == 'servicesales':
        return Factor.objects.filter(service__provider_id=partner_id)


def home_page_report():
    """
    :return: tedade bazdide safheye asli
    """
    h = Report.objects.get(pk=1)
    return h.home_page_viewed

def get_staffs(*args, **kwargs):
    '''
    gets input via kwargs:
    :active_only: only shows active users
    :inactive_only: only shows inactive users
    :agency_only: only shows agencies
    :non_agency_only: only shows service providers
    :staff_name: filters staff by passed name(defaults on begin with)
    :staff_name_contains: filters staff by passed name(defaults on contains)
    :staff_id: returns the exact staff
    :sort_by: specify an ordering(Django standard string input)
    '''
    if staff_id:
        return SepasUser.objects.get(pk=staff_id)
    else:
        users = SepasUser.objects.filter(is_staff_member=True)
        if active_only:
            users = users.filter(Staff_Info__staff_is_active=True)
        elif inactive_only:
            users = users.filter(Staff_Info__staff_is_active=False)
        if agency_only:
            users = users.filter(Staff_Info__is_agency=True)
        elif non_agency_only:
            users = users.filter(Staff_Info__is_agency=False)
        if staff_name:
            users = users.filter(Leg_Info__company_name__startswith=staff_name)
        elif staff_name_contains:
            users = users.filter(Leg_Info__company_name__contains=staff_name_contains)
        if sort_by:
            users = users.order_by(sort_by)
        return users

def get_tickets(staff):
    '''
    gets staff object
    returns corresponding tickets
    '''
    if staff.staff_info.is_agency:
        return Ticket.objects.filter(Ticket__addresse__tours__provider=staff)
    else:
        return Ticket.objects.filter(Ticket__addresse__services__provider=staff)

def request_data(staff):
    db_user = staff.staff_info.db_info.db_user
    db_password = staff.staff_info.db_info.db_password
    db_name = staff.staff_info.db_info.db_name
    address = staff.staff_info.db_info.db_url
    data = {'db_user': db_user, 'db_password': db_password, 'db_name': db_name}
    body_data = urllib.parse.urlencode(data)
    h = httplib2.Http()
    resp, content = h.request(address, 'POST', body_data, {'Content-Type': 'application/x-www-form-urlencoded'})
    json_dump = simplejson.loads('[%s]' % content.decode("utf-8"))
    for record in json_dump:
        new_service, created = Service.objects.get_or_create(**record)
        if created:
            staff.services.add(new_service)
    staff.save()














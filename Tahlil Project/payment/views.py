﻿"""
 baad az dorost shodan DB tour
 inja data constant tabdil be data DB mishe ke ye jay gozari sadas :)
"""

from django.shortcuts import render
from django.http import HttpRequest, Http404, HttpResponseRedirect
from django.template import RequestContext
from django.contrib.auth.decorators import login_required, user_passes_test
from django.forms.formsets import formset_factory
from datetime import datetime
from payment.models import *
from toursNservices.models import *
from payment.forms import PeopleForm ,BankForm
from Profile.tests import RestrictedUser


@login_required
@user_passes_test(RestrictedUser)
def payment(request,id):
    """
    :param request: clicked on tour thumbnail
    :return: tour details page
    """
    #if Payment.objects.filter(user=request.user):
    #    user = Payment.objects.get(user=request.user)
    #    if user.is_payment:
    #        return render('payment/busy.html', context_instance=RequestContext(request))

    #    user.is_payment = True
    #    user.save()
    #    return HttpResponseRedirect('/payment/factor1/'+id)

    #Payment.objects.create(user=request.user, is_payment=True, count=0, product=int(id))
    #return HttpResponseRedirect('/payment/factor1/'+id)
    PeopleFormSet = formset_factory(PeopleForm)
    people_formset = PeopleFormSet(prefix='pfs')
    return render(request, 'payment/shopping-wizard.html', context_instance=RequestContext(request, {'people_formset' : people_formset}))


@login_required
@user_passes_test(RestrictedUser)
def cancel(request):
    if Payment.objects.filter(user=request.user):
        user =Payment.objects.get(user=request.user)
        user.is_payment = False
        user.save()

        return render('payment/cancle_payment.html', context_instance=RequestContext(request))
    return HttpResponseRedirect('/')

@login_required
@user_passes_test(RestrictedUser)
def factor1(request, id):

    if Payment.objects.filter(user=request.user):
        user = Payment.objects.get(user=request.user)
        if user.is_payment and user.product == int(id):
            tour = Tour.objects.get(id=id)
            tax = tour.price * 0.08
            final_price = tour.price + tax

            if request.method == 'POST':
                if request.POST.get('count','') == '':
                    msgError = 'برای خرید لطفا تعداد را وارد نمایید'
                    return render('payment/factor1.html', {'product': tour, 'tax': tax, 'final_price': final_price, 'id_product': id, 'error': True, 'msgError': msgError}, context_instance=RequestContext(request) )
                else:
                    user.count = request.POST.get('count')
                    user.save()
                    return HttpResponseRedirect('/payment/people/'+id)

            return render('payment/factor1.html', {'product': tour, 'tax': tax, 'final_price': final_price, 'id_product': id}, context_instance=RequestContext(request))
    return HttpResponseRedirect('/payment/'+id)

@login_required
@user_passes_test(RestrictedUser)
def people(request,id):

    if Payment.objects.filter(user=request.user):
        user = Payment.objects.get(user=request.user)

        if user.is_payment and user.product == int(id):
            tour = Tour.objects.get(id=id)
            form = PeopleForm()
            num = 1
            if request.method == 'POST':
                form = PeopleForm(request.POST)
                num = int(request.POST.get('num'))
                if form.is_valid():
                    form.save()
                    if num == 1:
                        user.people_id ='{}'.format(People.objects.get(id_num=request.POST.get('id_num')).id)

                    else:
                        user.people_id = user.people_id+'-{}'.format(People.objects.get(id_num=request.POST.get('id_num')).id)
                    user.save()
                    num = int(num) + 1
                    if num > user.count:
                        return HttpResponseRedirect('/payment/bank/'+id)
                    form = PeopleForm()

            return render('payment/people.html', {'num': num, 'product': tour, 'form':form, 'id_product': id}, context_instance=RequestContext(request))

    return HttpResponseRedirect('/payment/'+id)



def bank(request,id):
    if Payment.objects.filter(user=request.user):
        user = Payment.objects.get(user=request.user)
        if user.is_payment and user.product == int(id):
            tour = Tour.objects.get(id=id)
            form = BankForm()
            if request.method == 'POST':
                form = BankForm(request.POST)
                if form.is_valid():
                    return HttpResponseRedirect('/payment/factor2/'+id)

        return render('payment/bank.html', {'product': tour, 'form':form, 'id_product': id}, context_instance=RequestContext(request))

    return HttpResponseRedirect('/payment/'+id)


#
@login_required
@user_passes_test(RestrictedUser)
def factor2(request,id):
    if Payment.objects.filter(user=request.user):
        user = Payment.objects.get(user=request.user)
        if user.is_payment and user.product == int(id):
            tour = Tour.objects.get(id=id)

            cost = tour.price
            count = user.count
            tax = 0.08
            discount = 0
            final_cost = cost + cost*tax

            factor = Factor(user=request.user, product=tour.name, cost=cost, count=count, tax=tax, discount=discount, final_cost=final_cost)
            factor.save()

            people_user = user.people_id.split('-')

            for p in people_user:
                factor.people.add(People.objects.get(id=p))
            factor.save()

            user.is_payment=False
            user.save()
            return render('payment/factor2.html', {'product': tour, 'factor': factor, 'tax': (tax*100), 'tax_cost': (tax*cost), 'id_product': id}, context_instance=RequestContext(request))

    return HttpResponseRedirect('/payment/'+id)


def get_product(kind):
    if kind == 'tour':
        return Tour

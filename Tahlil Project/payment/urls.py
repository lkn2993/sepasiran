from django.conf.urls import url

urlpatterns = [

     url(r'^(?P<id>\d+)$', 'payment.views.payment', name='payment'),
     url(r'^factor1/(?P<id>\d+)$', 'payment.views.factor1', name='factor1'),
     url(r'^people/(?P<id>\d+)$', 'payment.views.people', name='people'),
     url(r'^bank/(?P<id>\d+)$', 'payment.views.bank', name='bank'),
     url(r'^factor2/(?P<id>\d+)$', 'payment.views.factor2', name='factor2'),
     url(r'^cancel/$', 'payment.views.cancel', name='cancel'),
]
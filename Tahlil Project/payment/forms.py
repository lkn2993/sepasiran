﻿"""
Definition of forms.
"""

from django import forms
from django.utils.translation import ugettext_lazy as _
from Profile.models import SepasUser, Add_Info

class PeopleForm(forms.Form):
    fname = forms.CharField(max_length=254, label = 'نام',
                               widget=forms.TextInput({
                                   #'class' : 'form-control form-control-solid',
                                   #'placeholder' : 'نام',
                                   'name' : 'fname'}))
    lname = forms.CharField(max_length=254, label = 'نام خانوادگی',
                               widget=forms.TextInput({
                                   #'class' : 'form-control form-control-solid',
                                   #'placeholder' : 'نام خانوادگی',
                                   'name' : 'lname'}))
    gender = forms.ChoiceField(label = 'جنسیت', choices=(('True', 'مذکر'), ('False', 'مونث')),
                               widget=forms.Select({
                                   #'class' : 'form-control form-control-solid',
                                   'name' : 'gender'}))
    date_of_birth = forms.DateField(label = 'تاریخ تولد',
                               widget=forms.DateInput({
                                   #'class' : 'form-control form-control-solid',
                                   #'placeholder' : 'تاریخ تولد',
                                   'name' : 'date_of_birth'})) ##TODO:Enter Persian Date Picker
    national = forms.CharField(max_length=254, label = 'کد ملی',
                               widget=forms.NumberInput({
                                   #'class' : 'form-control form-control-solid',
                                   #'placeholder' : 'کد ملی',
                                   'name' : 'national'}))
    id_num = forms.CharField(max_length=254, label = 'شماره شناسنامه',
                               widget=forms.NumberInput({
                                   #'class' : 'form-control form-control-solid',
                                   #'placeholder' : 'شماره شناسنامه',
                                   'name' : 'id_num'}))
    cell = forms.CharField(max_length=254, label = 'شماره تماس',
                               widget=forms.NumberInput({
                                   #'class' : 'form-control form-control-solid',
                                   #'placeholder' : 'شماره تماس',
                                   'name' : 'cell'}))
    passport = forms.ImageField(label = 'اسکن پاسپورت(در صورتی که سفر خارجی است)',
                               widget=forms.FileInput({
                                   #'class' : 'form-control form-control-solid',
                                   #'placeholder' : 'اسکن پاسپورت(در صورتی که سفر خارجی است)',
                                   'name' : 'passport'})) ##TODO:Enter Persian Date Picker


    def save(self, commit=True):
        if not (SepasUser.objects.filter(email=self.fname + '_' + self.fname + '@sepasuser.com').first()):
            if not (Add_Info.objects.filter(national = self.national).first()):
                if self.cleaned_data['fname'] and self.cleaned_data['lname'] and self.cleaned_data['gender'] and self.cleaned_data['date_of_birth']:
                    if self.cleaned_data['national'] and self.cleaned_data['id_num'] and self.cleaned_data['cell']:
                        new_user = SepasUser.objects.create_user(self.fname, self.lname, self.fname + '_' + self.fname + '@sepasuser.com', self.date_of_birth)
                        new_user.gender = self.gender
                        new_user.is_active = False
                        if self.cleaned_data['passport']:
                            add_info = Add_Info(national = self.national, id_num = self.id_num, cell = self.cell, passport = self.passport)
                        else:
                            add_info = Add_Info(national = self.national, id_num = self.id_num, cell = self.cell)
                        add_info.save()
                        new_user.add_info = add_info
                        new_user.save()

class BankForm(forms.Form):

    card_name = forms.CharField(max_length=100,min_length=1,required=True,widget=forms.TextInput(attrs={'class':'form-control'}))
    card_name.label = 'نام دارنده ی کارت '
    card_number = forms.CharField(max_length=16,min_length=16,required=True,widget=forms.TextInput(attrs={'class':'form-control'}))
    card_number.label='شماره کارت '
    card_cvc = forms.CharField(max_length=4,min_length=3,required=True,widget=forms.TextInput(attrs={'class':'form-control'}))
    card_cvc.label = 'CVV2'
    card_expiry_date = forms.CharField(max_length=7,min_length=7,required=True,widget=forms.TextInput(attrs={'class':'form-control'}))
    card_expiry_date.label='تاریخ انقضا'




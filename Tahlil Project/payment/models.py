﻿# written by Fatemeh Sadeghi
from django.db import models
from Profile.models import SepasUser
from toursNservices.models import Tour, Service
# Create your models here.
class Factor(models.Model):
    users = models.ManyToManyField(SepasUser)
    services = models.ManyToManyField(Service, blank=True)
    VAT = models.FloatField(default=0.05, editable=False)
    total_cost = models.FloatField(editable=False)
    date = models.DateTimeField(auto_now_add=True)
    payment_recieved = models.BooleanField(default=False, editable=False)
    payment_settled = models.BooleanField(default=False, editable=False)
    def save(self, *args, **kwargs):
        TC = 0
        for service in self.services:
            TC += service.price * (1 - service.discount)
        self.total_cost = TC * len(users) * (1 + VAT)
        super(Factor, self).save(*args, **kwargs)
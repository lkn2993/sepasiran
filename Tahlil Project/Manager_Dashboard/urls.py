from django.conf.urls import url

urlpatterns = [
    url(r'^$', 'Manager_Dashboard.views.start', name='start'),
    url(r'^coworker/$', 'Manager_Dashboard.views.coworker', name='coworker'),
    url(r'^coworker/details/$', 'Manager_Dashboard.views.coworker_details', name='coworker_details'),
    url(r'^finance/$', 'Manager_Dashboard.views.finance', name='finance'),
    url(r'^search/$', 'Manager_Dashboard.views.search', name='search'),
    url(r'^users/$', 'Manager_Dashboard.views.users', name='users'),
]
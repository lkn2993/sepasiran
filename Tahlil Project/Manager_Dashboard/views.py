﻿# Create your views here.

from django.shortcuts import render
from django.http import HttpRequest
from django.template import RequestContext
from datetime import datetime
from django.contrib.auth.decorators import login_required, user_passes_test
from Profile.tests import AdminUser

@login_required
@user_passes_test(AdminUser)
def start(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    active = ['inactive', 'inactive', 'inactive', 'inactive', 'inactive']
    active[0] = 'active'
    return render(
        request,
        'Manager_Dashboard/MD_start.html',
        context_instance=RequestContext(request, { 'title':'داشبورد مدیریتی', 'active' : active })
    )

@login_required
@user_passes_test(AdminUser)
def finance(request):
    """

    """
    active = ['inactive', 'inactive', 'inactive', 'inactive', 'inactive']
    active[1] = 'active'
    assert isinstance(request, HttpRequest)
    return render(request, 'Manager_Dashboard/MD_Finance.html', context_instance=RequestContext(request, { 'title':'گزارش مالی', 'active' : active }))

@login_required
@user_passes_test(AdminUser)
def coworker(request):
    """

    """
    assert isinstance(request, HttpRequest)
    active = ['inactive', 'inactive', 'inactive', 'inactive', 'inactive']
    active[2] = 'active'
    return render(request, 'Manager_Dashboard/MD_coworker.html', context_instance=RequestContext(request, { 'title':'گزارش همکاران', 'active' : active }))

@login_required
@user_passes_test(AdminUser)
def coworker_details(request):
    """

    """
    assert isinstance(request, HttpRequest)
    active = ['inactive', 'inactive', 'inactive', 'inactive', 'inactive']
    active[2] = 'active'
    return render(request, 'Manager_Dashboard/MD_coworkerـdetail.html', context_instance=RequestContext(request, { 'title':'گزارش همکاران', 'active' : active }))

@login_required
@user_passes_test(AdminUser)
def users(request):
    """

    """
    active = ['inactive', 'inactive', 'inactive', 'inactive', 'inactive']
    active[3] = 'active'
    assert isinstance(request, HttpRequest)
    return render(request, 'Manager_Dashboard/MD_Users.html', context_instance=RequestContext(request, { 'title':'گزارش کاربران', 'active' : active }))

@login_required
@user_passes_test(AdminUser)
def search(request):
    """

    """
    active = ['inactive', 'inactive', 'inactive', 'inactive', 'inactive']
    active[4] = 'active'
    assert isinstance(request, HttpRequest)
    return render(request, 'Manager_Dashboard/MD_Search.html', context_instance=RequestContext(request, { 'title':'گزارش جستجو', 'active' : active }))


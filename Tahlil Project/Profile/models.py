﻿from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)
from django.core.files import File

#### user: lkn2993@gmail.com pas: aliali####

class SepasUserManager(BaseUserManager):
    def create_user(self, f_name, l_name, email, date_of_birth, password=None):
        """

        """
        if not email:
            raise ValueError('نوشتن آدرس ایمیل اجباری است')

        user = self.model(
            email=self.normalize_email(email),
            f_name=f_name,
            l_name=l_name,
            date_of_birth=date_of_birth,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, f_name, l_name, email, date_of_birth, password):
        """

        """
        user = self.create_user(f_name,
            l_name,
            email,
            date_of_birth,
            password,
        )
        user.is_admin = True
        user.is_staff_member = True
        user.save(using=self._db)
        return user


class Add_Info(models.Model):
    national = models.CharField(max_length=20, primary_key = True, verbose_name='کد ملی')
    id_num = models.CharField(max_length=20, verbose_name='شماره شناسنامه')
    phone = models.CharField(max_length=20, blank = True, null = True, verbose_name='تلفن')
    cell = models.CharField(max_length=20, blank = True, null = True, verbose_name='همراه')
    postal = models.CharField(max_length=20, blank = True, null = True, verbose_name='کد پستی')
    address = models.TextField(verbose_name='آدرس', blank = True, null = True,)
    about = models.TextField(verbose_name='توضیحات کاربر', blank = True, null = True,)
    passport = models.ImageField(blank = True, null = True, verbose_name='پاسپورت')
    cons_stat = models.CharField(max_length=20, blank = True, null = True, verbose_name='وضعیت خدمت')
    def __str__(self):              
        return self.national
    class Meta:
        verbose_name='اطلاعات شخصی'
        verbose_name_plural='اطلاعات شخصی'

class Bank_Info(models.Model):
    shaba = models.CharField(max_length = 40, unique = True, verbose_name='شماره شبا')
    card = models.CharField(max_length=20, blank = True, verbose_name='شماره کارت شتاب')
    bank = models.CharField(max_length=20, verbose_name='بانک صادر کننده')
    def __str__(self):              
        return self.shaba
    class Meta:
        verbose_name='اطلاعات حساب بانکی'
        verbose_name_plural='اطلاعات حسابهای بانکی'

class Leg_Info(models.Model):
    company_name = models.CharField(max_length=30, verbose_name='نام شرکت',)
    eco_num = models.CharField(max_length=30, verbose_name='کد اقتصادی',)
    com_date = models.DateField(verbose_name='تاریخ تاسیس شرکت')
    com_phone = models.CharField(max_length=20, verbose_name='شماره تماس شرکت')
    com_postal = models.CharField(max_length=20, verbose_name='کد پستی شرکت')
    address = models.TextField(verbose_name='آدرس شرکت')
    website = models.URLField(verbose_name='آدرس وبسایت شرکت')
    def __str__(self):              
        return self.company_name
    class Meta:
        verbose_name='اطلاعات حقوقی'
        verbose_name_plural='اطلاعات حقوقی'

class DB_Info(models.Model):
    db_url = models.URLField(verbose_name='آدرس دسترسی')
    db_user = models.CharField(max_length=20, verbose_name='نام کاربری')
    db_password = models.CharField(max_length=20, verbose_name='گذرواژه')
    db_name = models.CharField(max_length=20, verbose_name='نام پایگاه داده')
    def __str__(self):              
        return self.db_user
    class Meta:
        verbose_name='اطلاعات پایگاه داده'
        verbose_name_plural='اطلاعات پایگاه های داده'

class Staff_Info(models.Model):
    is_agency = models.BooleanField(default=True, verbose_name='گردشساز')
    work_base = models.CharField(max_length=40, verbose_name='زمینه کاری')
    licence = models.ImageField(upload_to='images/', verbose_name='جواز کسب و کار')
    staff_about = models.TextField(verbose_name='توضیحات کسب و کار')
    staff_is_active = models.BooleanField(default=False, verbose_name='فعال بودن وضعیت همکاری')
    db_info = models.OneToOneField(DB_Info, null=True, verbose_name='اطلاعات پایگاه داده')
    def __str__(self):              
        return self.work_base
    class Meta:
        verbose_name='اطلاعات همکار'
        verbose_name_plural='اطلاعات همکاران'

class SepasUser(AbstractBaseUser):
    email = models.EmailField(
        verbose_name='آدرس ایمیل',
        max_length=255,
        unique=True,
    )
    date_of_birth = models.DateField(verbose_name='تاریخ تولد',)
    creation_date = models.DateTimeField(auto_now_add=True, verbose_name='تاریخ ساخت',)
    f_name = models.CharField(max_length=30, verbose_name='نام',)
    l_name = models.CharField(max_length=30, verbose_name='نام خانوادگی',)
    avatar = models.ImageField(upload_to='images/', verbose_name='عکس', default = 'images/avatar.png')
    gender = models.BooleanField(default = True)
    add_info = models.OneToOneField(Add_Info, null=True, blank=True, verbose_name='اطلاعات افزوده')
    bank_info = models.OneToOneField(Bank_Info, null=True, blank=True, verbose_name='اطلاعات حساب بانکی')
    leg_info = models.OneToOneField(Leg_Info, null=True, blank=True, verbose_name='اطلاعات حقوقی')
    staff_info = models.OneToOneField(Staff_Info, null=True, blank=True, verbose_name='اطلاعات همکار')
    is_active = models.BooleanField(default=True, verbose_name='فعال',)
    is_staff_member = models.BooleanField(default=False, verbose_name='همکار',)
    is_admin = models.BooleanField(default=False, verbose_name='مدیر',)
    is_news_member = models.BooleanField(default=False, verbose_name='عضویت خبرنامه')

    objects = SepasUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['date_of_birth', 'f_name', 'l_name']

    def get_full_name(self):
        return self.f_name + ' ' + self.l_name

    def get_short_name(self):
        return self.email

    def __str__(self):              
        return self.email + ':' + self.f_name + ' ' + self.l_name

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        return True
    @property
    def is_staff(self):
        "Is the user a member of staff?"
        return self.is_staff_member 
    class Meta:
        verbose_name='حساب کاربری'
        verbose_name_plural='حساب های کاربری'

class Ticket(models.Model):
    owner = models.ForeignKey(SepasUser, verbose_name='صاحب تیکت', related_name='Ticket_owner')
    addresse = models.ForeignKey('payment.Factor', verbose_name='فاکتور مطابق با تیکت', related_name='Ticket_adresse')
    topic = models.CharField(max_length=254, verbose_name='موضوع تیکت')
    class Meta:
        verbose_name='تیکت پشتیبانی'
        verbose_name_plural='تیکت های پشتیبانی'

class Answer(models.Model):
    ticket = models.ForeignKey(Ticket, verbose_name='تیکت مربوطه')
    user = models.ForeignKey(SepasUser, verbose_name='کاربر مربوطه')
    date = models.DateTimeField(auto_now_add=True, verbose_name='تاریخ ثبت')
    content = models.CharField(max_length=254, verbose_name='محتوا')
    class Meta:
        verbose_name='جواب'
        verbose_name_plural='جوابها'

class Notifications(models.Model):
    owner = models.ForeignKey(SepasUser, verbose_name='صاحب خبر')
    note_type = models.CharField(max_length=254, verbose_name='نوع خبر')
    description = models.CharField(max_length=254, verbose_name='محتوای خبر')
    class Meta:
        verbose_name='خبر'
        verbose_name_plural='اخبار'

class FAQ(models.Model):
    question = models.CharField(max_length=254, verbose_name='سوال')
    answer = models.CharField(max_length=254, verbose_name='جواب')
    class Meta:
        verbose_name='سوال معمول'
        verbose_name_plural='سوالات معمول'
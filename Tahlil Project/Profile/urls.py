﻿from django.conf.urls import url

urlpatterns = [
    url(r'^$', 'Profile.views.home', name='home'),
    url(r'^settings/$', 'Profile.views.settings', name='settings'),
    url(r'^support/(?P<factor_id>.*)$', 'Profile.views.support', name='support'),
]
import datetime, time

from django.contrib.admin.widgets import AdminFileWidget
from django.forms.fields import EMPTY_VALUES
from django.forms.widgets import TextInput
from django.forms.util import flatatt
from django.utils import formats
from django.utils.translation import ugettext as _
from django.utils.safestring import mark_safe
from app import jalali
import os
from PIL import Image

class SepasImageInput(AdminFileWidget):
    def render(self, name, value, attrs=None):
        output = []
        if value and getattr(value, "url", None):

            image_url = value.url
            file_name=str(value)

            # defining the size
            size='100x100'
            x, y = [int(x) for x in size.split('x')]
            try :
                # defining the filename and the miniature filename
                filehead, filetail  = os.path.split(value.path)
                basename, format        = os.path.splitext(filetail)
                miniature                   = basename + '_' + size + format
                filename                        = value.path
                miniature_filename  = os.path.join(filehead, miniature)
                filehead, filetail  = os.path.split(value.url)
                miniature_url           = filehead + '/' + miniature

                # make sure that the thumbnail is a version of the current original sized image
                if os.path.exists(miniature_filename) and os.path.getmtime(filename) > os.path.getmtime(miniature_filename):
                    os.unlink(miniature_filename)

                # if the image wasn't already resized, resize it
                if not os.path.exists(miniature_filename):
                    image = Image.open(filename)
                    image.thumbnail([x, y], Image.ANTIALIAS)
                    try:
                        image.save(miniature_filename, image.format, quality=100, optimize=1)
                    except:
                        image.save(miniature_filename, image.format, quality=100)

                output.append(u' <div><a href="%s" target="_blank"><img src="%s" alt="%s" /></a></div> %s ' % \
                (miniature_url, miniature_url, miniature_filename, _('Change:')))
            except:
                pass
        output.append(super(AdminFileWidget, self).render(name, value, attrs))
        return mark_safe(u''.join(output))
 

class SepasDateInput(TextInput):
    class Media:
        css = {
            'all': ('/static/content/persian-datepicker-0.4.5.css',)
        }
        js = (
              '/static/scripts/persian-date.js',
              '/static/scripts/persian-datepicker-0.4.5.js',
              '/static/scripts/persianDateLoad.js',
        )
 
    dformat = '%Y-%m-%d'
    def render(self, name, value, attrs=None):
        if value is None: value = ''
        final_attrs = self.build_attrs(attrs, type=self.input_type, name=name)
        if value != '':
            try:
                final_attrs['value'] = value.strftime(self.dformat)
            except:
                final_attrs['value'] = value
        if not 'id' in final_attrs:
            final_attrs['id'] = u'%s_id' % (name)
 
        jsdformat = self.dformat
        custom_class = final_attrs.get('class', None)
        final_attrs.pop('class', None)
        parsed_atts = flatatt(final_attrs)
        a = u'<input class="%s observer" input_formats="[%s]" %s> %s' % (custom_class, self.dformat, parsed_atts, self.media)
        return mark_safe(a)
 
    def value_from_datadict(self, data, files, name):
 
        value = data.get(name, None)
        if value in EMPTY_VALUES:
            return None
        if isinstance(value, datetime.datetime):
            return value
        if isinstance(value, datetime.date):
            d = jalali.Persian(value.year, value.month, value.day).gregorian_datetime()
            return datetime.combine(d, time())
        else:
            try:
                d = jalali.Persian(value).gregorian_datetime()
                return d
            except ValueError:
                return None
        return None
 
    def _has_changed(self, initial, data):
        """
        Return True if data differs from initial.
        Copy of parent's method, but modify value with strftime function before final comparsion
        """
        if data is None:
            data_value = u''
        else:
            data_value = data
 
        if initial is None:
            initial_value = u''
        else:
            initial_value = initial 
        return False
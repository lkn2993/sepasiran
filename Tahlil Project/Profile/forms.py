﻿from django import forms
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from .models import Add_Info, SepasUser, Bank_Info, Leg_Info, DB_Info, Staff_Info, Ticket, Answer
from .widgets import SepasImageInput, SepasDateInput
from toursNservices.models import Place
class SepasAuthenticationForm(AuthenticationForm):
    """Authentication form which uses boostrap CSS."""
    email = forms.EmailField(
                               widget=forms.EmailInput({
                                   'class' : 'form-control form-control-solid',
                                   'placeholder' : 'نام کاربری یا آدرس ایمیل',
                                   'name' : 'email'}))
    password = forms.CharField(max_length=254,
                               widget=forms.PasswordInput({
                                   'class' : 'form-control form-control-solid',
                                   'placeholder' : 'گذرواژه',
                                   'name' : 'password'}))


class SepasCreationForm(UserCreationForm):
    fname = forms.CharField(max_length=254,
                               widget=forms.TextInput({
                                   'placeholder' : 'نام',
                                   'name' : 'fname'}))
    lname = forms.CharField(max_length=254,
                               widget=forms.TextInput({
                                   'placeholder' : 'نام خانوادگی',
                                   'name' : 'lname'}))
    gender = forms.ChoiceField(choices=(('gender', 'جنسیت'), ('True', 'مذکر'), ('False', 'مونث')),
                               widget=forms.Select({
                                   'name' : 'gender'}))
    date_of_birth = forms.DateField(
                               widget=SepasDateInput({
                                   'placeholder' : 'تاریخ تولد',
                                   'name' : 'date_of_birth'}))
    email = forms.EmailField(
                               widget=forms.EmailInput({
                                   'placeholder' : 'نام کاربری یا آدرس ایمیل',
                                   'name' : 'email'}))
    password = forms.CharField(max_length=254,
                               widget=forms.PasswordInput({
                                   'placeholder' : 'گذرواژه',
                                   'name' : 'password',
                                   'id'   : 'register_password'}))
    password2 = forms.CharField(max_length=254,
                               widget=forms.PasswordInput({
                                   'placeholder' : 'گذرواژه (مجدد)',
                                   'name' : 'password2'}))
    is_staff = forms.BooleanField(widget=forms.CheckboxInput({
                                   'name' : 'is_staff'}))
    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("رمز عبور با تکرار حود مطابقت نمی کند")
        return password2
    def __init__(self, *args, **kwargs):
        super(SepasCreationForm, self).__init__(*args, **kwargs)
        for field in self: 
            field.field.widget.attrs['class'] = 'form-control form-control-solid'

class InfoForm(forms.ModelForm):
    class Meta:
        model = Add_Info
        exclude=['passport', 'cons_stat']
        widgets = {'phone': forms.NumberInput(),
                   'cell': forms.NumberInput(),
                   'national': forms.NumberInput(),
                   'id_num': forms.NumberInput(),
                   'address': forms.Textarea(),
                   'postal': forms.NumberInput(),
                   'about': forms.Textarea(),
                   }
    def __init__(self, *args, **kwargs):
        super(InfoForm, self).__init__(*args, **kwargs)
        for field in self: 
            field.field.widget.attrs['class'] = 'form-control'

class P_InfoForm(forms.ModelForm):
    class Meta:
        model = SepasUser
        fields = ['f_name', 'l_name', 'date_of_birth', 'avatar', 'is_news_member']
        widgets = {'date_of_birth': SepasDateInput(),
                   'avatar': SepasImageInput(),
                   }
    def __init__(self, *args, **kwargs):
        super(P_InfoForm, self).__init__(*args, **kwargs)
        for field in self: 
            field.field.widget.attrs['class'] = 'form-control'

class F_InfoForm(forms.ModelForm):
    class Meta:
        model = Add_Info
        fields = ['passport', 'cons_stat']
        widgets = { 'passport': SepasImageInput(),
                   }
    def __init__(self, *args, **kwargs):
        super(F_InfoForm, self).__init__(*args, **kwargs)
        for field in self: 
            field.field.widget.attrs['class'] = 'form-control'

class BankForm(forms.ModelForm):
    class Meta:
        model = Bank_Info
        fields = '__all__'
        widgets = {'shaba': forms.NumberInput(),
                   'card': forms.NumberInput(),
                   }
    def __init__(self, *args, **kwargs):
        super(BankForm, self).__init__(*args, **kwargs)
        for field in self: 
            field.field.widget.attrs['class'] = 'form-control'

class LegalForm(forms.ModelForm):
    class Meta:
        model = Leg_Info
        fields = '__all__'
        widgets = {'com_date': SepasDateInput(),
                   'com_phone': forms.NumberInput(),
                   'com_postal': forms.NumberInput(),
                   }
    def __init__(self, *args, **kwargs):
        super(LegalForm, self).__init__(*args, **kwargs)
        for field in self: 
            field.field.widget.attrs['class'] = 'form-control'

class PassChange(forms.Form):
    password = forms.CharField(max_length=254,
                               widget=forms.PasswordInput({
                                   'class' : 'form-control',
                                   'placeholder' : 'گذرواژه',
                                   'name' : 'password'}))
    password2 = forms.CharField(max_length=254,
                               widget=forms.PasswordInput({
                                   'class' : 'form-control',
                                   'placeholder' : 'گذرواژه (مجدد)',
                                   'name' : 'password2'}))
    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("رمز عبور با تکرار حود مطابقت نمی کند")
        return password2

class DBForm(forms.ModelForm):
    class Meta:
        model = DB_Info
        fields = '__all__'
        widgets = {'db_url': forms.URLInput(),
                   'db_user': forms.TextInput(),
                   'db_password': forms.PasswordInput(),
                   }
    def __init__(self, *args, **kwargs):
        super(DBForm, self).__init__(*args, **kwargs)
        for field in self: 
            field.field.widget.attrs['class'] = 'form-control'

class StaffForm(forms.ModelForm):
    class Meta:
        model = Staff_Info
        exclude = ['staff_is_active']
        widgets = {'staff_about': forms.Textarea(),
                   'licence': SepasImageInput(),
                   }
    def __init__(self, *args, **kwargs):
        super(StaffForm, self).__init__(*args, **kwargs)
        for field in self: 
            field.field.widget.attrs['class'] = 'form-control'

class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        fields = ['content']
        widgets = {'content': forms.Textarea(attrs={'class': 'form-control'}),                  
                   }

class TicketForm(forms.ModelForm):
    class Meta:
        model = Ticket
        fields = ['topic']
        widgets = {'topic': forms.RadioSelect(choices=(('staff', 'گردش سازان'),('tours', 'سفرها')),attrs={'class': 'form-control'}),                  
                   }

class LocationForm(forms.ModelForm):
    class Meta:
        model = Place
        fields = '__all__'
    def __init__(self, *args, **kwargs):
        super(LocationForm, self).__init__(*args, **kwargs)
        for field in self: 
            field.field.widget.attrs['class'] = 'form-control'
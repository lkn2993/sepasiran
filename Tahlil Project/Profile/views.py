﻿# Create your views here.

import ast

from app import jalali

from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpRequest, HttpResponse
from django.template import RequestContext
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth import authenticate, login as auth_login
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.forms.models import model_to_dict
from .models import SepasUser as UM, FAQ, Answer, Notifications
from CPRM.utils import request_data
from payment.models import Factor
from toursNservices.models import Tour
from .forms import (
    SepasAuthenticationForm as LoginForm, SepasCreationForm as UserForm, InfoForm, P_InfoForm, F_InfoForm,
    BankForm, LegalForm, PassChange, DBForm, StaffForm, TicketForm, AnswerForm
    )
from .tests import RestrictedUser
from datetime import datetime

active = []
next_url = None
def init_works(request):
    assert isinstance(request, HttpRequest)
    global current_user
    global about_user
    global active
    active = ['inactive', 'inactive', 'inactive']
    current_user = UM.objects.get(email=request.user.email)
    if current_user.add_info:
        about_user = current_user.add_info.about
    else:
        about_user = None
    global services_bought
    services_bought = 0
    global tours_registered
    tours_registered = 0

def sepas_user_submission(PostString, requested_form, requested_data, request):
    if request.POST.get(PostString, None):
        if requested_data:
            form = requested_form(request.POST, instance=requested_data)
        else:
            form = requested_form(request.POST)
        if form.is_valid():
            data = form.save(commit=False)
            data.save()
            return data
    return requested_data

def jsres(n_url):
    return '''<script type="text/javascript">var refreshParent = function() {
    parent.location.href = "''' + n_url + '''";
    return true;
    };
    refreshParent();
    window.location.href = "''' + n_url + '''";
    parent.$.fancybox.close();</script>
    '''

@login_required
def home(request):
    """Renders the home page."""
    init_works(request)
    active[0] = 'active'
    answers = Answer.objects.filter(ticket__owner=current_user).order_by('-date')
    user_factors = Factor.objects.filter(users__email=current_user.email)
    return render(
        request,
        'Profile/index.html',
        context_instance=RequestContext(request, { 'title':'حساب کاربری', 'active' : active, 'user' : current_user,
                                                  'about_user' : about_user, 'services_bought' : services_bought, 'tours_registered' : tours_registered,
                                                  'answers' : answers, 'user_factors' : user_factors})
    )

@login_required
def settings(request):
    """

    """
    init_works(request)
    active[1] = 'active'
    if request.method == 'POST':

        if request.POST.get('f_name', None):
            info_form = P_InfoForm(request.POST, instance=current_user)
            if info_form.is_valid():
                info_form.save()
        current_user.add_info = sepas_user_submission('phone', InfoForm, current_user.add_info, request)
        current_user.bank_info = sepas_user_submission('shaba', BankForm, current_user.bank_info, request)
        current_user.add_info = sepas_user_submission('cons_stat', F_InfoForm, current_user.add_info, request)
        current_user.leg_info = sepas_user_submission('company_name', LegalForm, current_user.leg_info, request)
        current_user.staff_info = sepas_user_submission('licence', StaffForm, current_user.staff_info, request)
        current_staff = current_user.staff_info
        if current_staff:
            current_staff.db_info = sepas_user_submission('db_url', DBForm, current_staff.db_info, request)
        current_user.save()
        return redirect(reverse('Profile:settings'))


    if current_user.add_info:
        add_info_form = InfoForm(initial=model_to_dict(current_user.add_info, exclude=['passport', 'cons_stat']))
    else:
        add_info_form = InfoForm();
    info_form = P_InfoForm(initial=model_to_dict(current_user, fields=['f_name', 'l_name', 'date_of_birth', 'avatar', 'is_news_member']))
    pass_change_form = PassChange()
    if current_user.bank_info:
        bank_form = BankForm(initial=model_to_dict(current_user.bank_info))
    else:
        bank_form = BankForm()
    if current_user.add_info:
        foreign_info_form = F_InfoForm(initial={'passport' : current_user.add_info.passport,
                                        'cons_stat' : current_user.add_info.cons_stat,
                                        })
    else:
        foreign_info_form = F_InfoForm();
    if current_user.leg_info:
        legal_form = LegalForm(initial=model_to_dict(current_user.leg_info))
    else:
        legal_form = LegalForm()
    if current_user.staff_info:
        staff_form = StaffForm(initial={'work_base' : current_user.staff_info.work_base,
                                          'licence' : current_user.staff_info.licence,
                                          })
        if current_user.staff_info.db_info:
            db_form = DBForm(initial=model_to_dict(current_user.db_info))
        else:
            db_form = DBForm();
    else:
        staff_form = StaffForm()
        db_form = DBForm()
    return render(request, 'Profile/settings.html', { 'title':'تنظیمات حساب کاربری', 'active' : active, 'user' : current_user,
                                                      'info_form' : info_form, 'add_info_form' : add_info_form, 'pass_change_form' : pass_change_form,
                                                      'bank_form' : bank_form, 'foreign_info_form' : foreign_info_form, 'legal_form' : legal_form,
                                                      'staff_form' : staff_form, 'db_form' : db_form,
                                                      'about_user' : about_user, 'services_bought' : services_bought, 
                                                      'tours_registered' : tours_registered})

@login_required
def support(request, factor_id):
    """

    """
    assert isinstance(request, HttpRequest)
    init_works(request)
    active[2] = 'active'
    factor = get_object_or_404(Factor, pk=factor_id)
    if request.method == 'POST':
        ticket_form = TicketForm(request.POST)
        answer_form = AnswerForm(request.POST)
        if ticket_form.is_valid() and answer_form.is_valid():
            answer = answer_form.save(commit=False)
            answer.user = current_user
            ticket = ticket_form.save(commit=False)
            ticket.owner = current_user
            ticket.addresse = factor
            ticket.save()
            answer.ticket = ticket
            answer.save()
    ticket_form = TicketForm()
    answer_form = AnswerForm()
    faq = FAQ.objects.all()
    return render(request, 'Profile/support.html', { 'title':'پشتیبانی', 'active' : active, 'user' : current_user,
                                                     'about_user' : about_user, 'services_bought' : services_bought,
                                                     'tours_registered' : tours_registered,
                                                     'ticket_form' : ticket_form, 'answer_form' : answer_form, 'faq' : faq})

def login(request):
    """

    """
    assert isinstance(request, HttpRequest)
    global next_url
    if request.method == 'POST':
        if request.POST.get('fname', None):
            new_user = UM.objects.create_user(request.POST['fname'], request.POST['lname'],
                                              request.POST['email'], request.POST['date_of_birth'], request.POST['password2'])
            new_user.gender = ast.literal_eval(request.POST['gender'])
            new_user.is_staff_member = ('is_staff' in request.POST)
            new_user.date_of_birth = jalali.Persian(new_user.date_of_birth).gregorian_datetime()
            new_user.save()
            user = authenticate(email=request.POST['email'], password=request.POST['password2'])
            auth_login(request, user)
            return HttpResponse(jsres(next_url))
        else:
            user = authenticate(email=request.POST['email'], password=request.POST['password'])
            if user is not None:
                if user.is_active:
                    auth_login(request, user)
                    return HttpResponse(jsres(next_url))
                #else:
                # Return a 'disabled account' error message
            #else:
                # Return a 'password mismatch' error message

    else:
        next_url = request.GET.get('next', '/')
        new_user_form = UserForm()
        if 'dashboard' in next_url:
            form = LoginForm(initial={'email' : 'lkn2993@gmail.com'})
            login_template = 'Manager_Dashboard/MD_lock_screen.html'
        else:
            form = LoginForm()
            login_template = 'Profile/login.html'
        return render(request, login_template, { 'title' : 'ورود', 'form' : form, 'new_user_form' : new_user_form})


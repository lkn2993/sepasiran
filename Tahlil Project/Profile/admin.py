﻿from django import forms
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField

from .models import SepasUser, Add_Info, Bank_Info, Leg_Info, DB_Info, Staff_Info, Answer, Ticket, FAQ, Notifications


class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='گذرواژه', widget=forms.PasswordInput)
    password2 = forms.CharField(label='تکرار گذرواژه', widget=forms.PasswordInput)

    class Meta:
        model = SepasUser
        fields = ('email', 'date_of_birth', 'f_name', 'l_name')

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("رمز عبور با تکرار حود مطابقت نمی کند")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = SepasUser
        fields = ('email', 'password', 'date_of_birth', 'gender', 'f_name', 'l_name', 'avatar', 'add_info', 'bank_info', 'leg_info', 'is_active', 'is_admin', 'is_staff_member')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class SepasUserAdmin(UserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('email', 'date_of_birth', 'creation_date', 'gender', 'f_name', 'l_name', 'avatar', 'add_info', 'bank_info', 'leg_info', 'is_admin', 'is_staff_member')
    list_filter = ('is_admin', 'is_staff_member', 'is_active')
    fieldsets = (
        (None, {'fields': ('f_name', 'l_name', 'email', 'password')}),
        ('اطلاعات شخصی', {'fields': ('date_of_birth', 'gender',)}),
        ('اطلاعات اضافی', {'fields': ('add_info',)}),
        ('اطلاعات بانکی', {'fields': ('bank_info',)}),
        ('اطلاعات حقوقی', {'fields': ('leg_info',)}),
        ('اطلاعات همکار', {'fields': ('staff_info',)}),
        ('اجازه ها', {'fields': ('is_admin', 'is_staff_member')}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('f_name', 'l_name', 'email', 'date_of_birth', 'password1', 'password2')}
        ),
    )
    search_fields = ('l_name', 'email',)
    ordering = ('l_name', 'email',)
    filter_horizontal = ()

# Now register the new UserAdmin
admin.site.register(SepasUser, SepasUserAdmin)
# unregister the Group model from admin.
admin.site.unregister(Group)

admin.site.register(Add_Info)
admin.site.register(Bank_Info)
admin.site.register(Leg_Info)
admin.site.register(DB_Info)
admin.site.register(Staff_Info)
admin.site.register(Answer)
admin.site.register(Ticket)
admin.site.register(FAQ)
admin.site.register(Notifications)
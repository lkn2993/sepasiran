from django.forms import ModelForm, DateInput
from .models import Tour, Place
from Profile.widgets import SepasDateInput
class SearchFormLocation(ModelForm):
    class Meta:
        model = Place
        fields = ['city']

class SearchForm(ModelForm):
    class Meta:
        model = Tour
        fields = ['origin_city', 'city', 'start_date', 'end_date']
        widgets = {
            'start_date' : SepasDateInput({'class':'form-control'}),
            'end_date' : SepasDateInput({'class':'form-control'}),
            }
    def __init__(self, *args, **kwargs):
        super(SearchForm, self).__init__(*args, **kwargs)
        self.fields['origin_city'].widget.attrs.update({'class': 'form-control typeahead'})
        self.fields['city'].widget.attrs.update({'class': 'form-control typeahead'})


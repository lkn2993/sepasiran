﻿from django.contrib import admin
from django import forms
from .models import Tour, Service, Sojourn, Transportation, Comments, Images, TourCategory, Food
# Register your models here.
class ImagesInline(admin.StackedInline):
    model = Images
    extra = 3
    fieldsets = [(None, {'fields': ['image', 'caption', 'tour']})]


class TourAdmin(admin.ModelAdmin):
    fieldsets = [
        ('اطلاعات تور', {
            'fields': ['name', 'start_date', 'end_date', 'capacity', 'price', 'description', 'origin', 'location' ,'can_reserve', 'food_services',
                       'provider']}),
        ('اطلاعات اقامت', {'fields': ['sojourn']}),
        ('اطلاعات حمل و نقل', {'fields': ['transportation']}),
        ('نوع', {'fields': ['category']}),
    ]
    inlines = [ImagesInline]


admin.site.register(Tour, TourAdmin)
admin.site.register(Sojourn)
admin.site.register(Transportation)
admin.site.register(Comments)
admin.site.register(Images)
admin.site.register(TourCategory)
admin.site.register(Food)

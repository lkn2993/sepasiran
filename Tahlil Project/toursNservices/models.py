﻿import datetime
from django.db import models as m
from django.forms import DateInput

from Profile.models import SepasUser

class Place(m.Model):
    country = m.CharField(max_length=50, verbose_name='کشور')
    city = m.CharField(max_length=50, verbose_name='شهر')
    address = m.TextField(verbose_name='آدرس', blank=True, null=True)

    def __str__(self):
        return self.country + ', ' + self.city + ', ' + self.address

    class Meta:
        verbose_name = 'محل'
        verbose_name_plural = 'محل ها'

class Service(Place):
    name = m.CharField(max_length=60, verbose_name='نام ')
    price = m.IntegerField(verbose_name='قیمت')
    description = m.TextField(null=True, blank=True, verbose_name='توضیحات')
    provider = m.ForeignKey(SepasUser, verbose_name='ارائه دهنده', related_name='services', editable=False)
    creation_date = m.DateTimeField(auto_now_add=True, verbose_name='تاریخ ساخت',)
    start_date = m.DateTimeField(verbose_name='تاریخ شروع')
    end_date = m.DateTimeField(verbose_name='تاریخ اتمام', blank=True)
    duration = m.DateTimeField(verbose_name='مدت زمان', editable=False)
    adult_capacity = m.IntegerField(verbose_name='موجودی بزرگسال')
    child_capacity = m.IntegerField(verbose_name='موجودی خردسال', default=0, blank=True)
    capacity = m.IntegerField(verbose_name='موجودی کل', editable=False)
    views = m.IntegerField(verbose_name='تعداد بازدید', default=0, editable=False)
    created_date = m.DateTimeField(verbose_name='زمان ایجاد', auto_now_add=True)
    discount = m.IntegerField(default=0, blank=True, verbose_name='میزان تخفیف')
    rate = m.IntegerField(verbose_name='امتیاز', default=0, blank=True, editable=False)
    can_reserve = m.BooleanField(default=False, verbose_name='قابلیت رزرو', blank=True)
    related_tour = m.ForeignKey('Tour', blank=True, null=True, related_name='related_services')
    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.capacity = self.adult_capacity + self.child_capacity
        self.duration = self.end_date - self.start_date
        if not self.end_date:
            self.end_date = datetime.datetime.combine(self.start_date.date(), (23, 59))
        if self.provider.is_staff:
            return super(Service, self).save(*args, **kwargs)
        else:
            return "you are not staff!"

class TourCategory(m.Model):

    TOUR_CATEGURY = (
        ('TAFRIHI', 'تفریحی'),
        ('DAKHELI', 'داخلی'),
        ('KHAREJI','خارجی'),
        ('ZIARATI', 'زیارتی')
    )
    name = m.CharField(max_length=60, verbose_name=' نام دسته',choices=TOUR_CATEGURY)
    # logo = m.ImageField(upload_to='%Y/%m/%d', verbose_name='لوگو')
    # display = m.BooleanField(verbose_name='نمایش داده شود؟')
    # description = m.CharField(max_length=200, verbose_name='توضیحات')
    # # parent_categury = m.ForeignKey('self', default='home')
    # meta_title = m.CharField(max_length=60, verbose_name='عنوان متا')
    # # meta_keywords = m.ManyToManyField(tag, verbose_name='کلمات کلیدی')
    # users = m.ForeignKey(SepasUser, verbose_name='دسترسی گروهی کاربران')
    # friendly_URL = m.URLField(verbose_name='لینک', default=name)

    def __str__(self):

        return self.name


class Tour(Service):
    origin_country = m.CharField(max_length=50, verbose_name='کشور مبدا')
    origin_city = m.CharField(max_length=50, verbose_name='شهر مبدا')
    origin_address = m.TextField(verbose_name='آدرس مبدا', blank=True, null=True)
    offspring = m.OneToOneField(Place, verbose_name='مبدا', related_name='tour_offspring', editable=False)
    category = m.ManyToManyField(TourCategory, verbose_name='دسته')
    def __init__(self, *args, **kwargs):
        super(Tour, self).__init__(*args, **kwargs)
        self._meta.get_field('related_tour').editable = False
        self._meta.get_field('country').verbose_name = 'کشور مقصد'
        self._meta.get_field('city').verbose_name = 'شهر مقصد'
        self._meta.get_field('address').verbose_name = 'آدرس مقصد'
    class Meta:
        verbose_name = 'تور'
        verbose_name_plural = 'تور'
    def save(self, *args, **kwargs):
        new_loc = Place(country=self.origin_country, city=self.origin_city, address=self.origin_address)
        new_loc.save();
        self.offspring = new_loc
        return super(Tour, self).save(*args, **kwargs)

class Sojourn(Service):
    ONE_BED = 1
    TWO_BED_WITHOUT_CHILD = 2
    TWO_BED_WITH_CHILD = 2.5
    THREE_BED = 3
    FAMILY = 5
    BEDS = (
        (ONE_BED, 'یک  خوابه'),
        (TWO_BED_WITHOUT_CHILD, 'دو  خوابه'),
        (TWO_BED_WITH_CHILD, '  دو خوابه با بچه'),
        (THREE_BED, 'سه خوابه'),
        (FAMILY, 'سوییت خانوادگی 5 نفره '),
    )
    bed = m.FloatField(verbose_name='تعداد تخت', choices=BEDS, max_length=3, default=1)
    parking = m.BooleanField(verbose_name='پارکینگ')


class Transportation(Service):
    TRAIN = 'TR'
    AIRLINE = 'AI'
    AUTOBUS = 'AU'
    CRUISE = 'CR'
    TYPE = (
        (TRAIN, 'قطار'),
        (AIRLINE, 'هواپیما'),
        (AUTOBUS, 'اتوبوس'),
        (CRUISE, 'کشتی'),
    )
    KLASS = (
        ('economy', 'اقتصادی'),
        ('firstclass', 'درجه 1'),
    )
    DATE_INPUT_FORMATS = ['%Y-%m-%d',
                          '%m/%d/%Y',
                          '%m/%d/%y',
                          '%Y/%m/%d']
    origin = m.OneToOneField(Place, verbose_name='مبدا', related_name='transportation_origin', editable=False)
    origin_country = m.CharField(max_length=50, verbose_name='کشور مبدا')
    origin_city = m.CharField(max_length=50, verbose_name='شهر مبدا')
    origin_address = m.TextField(verbose_name='آدرس مبدا', blank=True, null=True)
    type = m.CharField(max_length=2, choices=TYPE, verbose_name='نوع وسیله نقلیه')
    price_type = m.CharField(max_length=10, verbose_name='کلاس', choices=KLASS)
    ticket_number = m.IntegerField(verbose_name='شماره بلیت')
    def save(self, *args, **kwargs):
        new_loc = Place(country=self.origin_country, city=self.origin_city, address=self.origin_address)
        new_loc.save();
        self.origin = new_loc
        return super(Transportation, self).save(*args, **kwargs)

class Attraction(Service):
    pass

#TODO: souvenir use a different model than service and should be probably removed or overhauled
#class Souvenir(Service):
#    DeliveryFee = m.IntegerField(verbose_name='هزینه ارسال')

#    def __str__(self):
#        return self.SouvenirName


# class tag(m.Model):
# content = m.CharField(max_length=30, null=True, blank=True)
# pass


class TourCategury(m.Model):
    TOUR_CATEGURY = (
        ('TAFRIHI', 'تفریحی'),
        ('DAKHELI', 'داخلی'),
        ('KHAREJI','خارجی'),
        ('ZIARATI', 'زیارتی')
    )
    name = m.CharField(max_length=60, verbose_name=' نام دسته',choices=TOUR_CATEGURY)
    # logo = m.ImageField(upload_to='%Y/%m/%d', verbose_name='لوگو')
    # display = m.BooleanField(verbose_name='نمایش داده شود؟')
    # description = m.CharField(max_length=200, verbose_name='توضیحات')
    # # parent_categury = m.ForeignKey('self', default='home')
    # meta_title = m.CharField(max_length=60, verbose_name='عنوان متا')
    # # meta_keywords = m.ManyToManyField(tag, verbose_name='کلمات کلیدی')
    # users = m.ForeignKey(SepasUser, verbose_name='دسترسی گروهی کاربران')
    # friendly_URL = m.URLField(verbose_name='لینک', default=name)



class Food(Service):
    # TODO: takmile food
    BREAKFAST = 'B'
    LAUNCH = 'L'
    DINNER = 'D'
    FOOD_SERVICES = (
        (BREAKFAST, 'صبحانه'),
        (LAUNCH, 'ناهار'),
        (DINNER, 'شام'),
    )
    food_services = m.CharField(max_length=1, choices=FOOD_SERVICES, verbose_name='وعده های غذایی')

    def __str__(self):
        return self.food_services







class Images(m.Model):
    image = m.ImageField(verbose_name='فایل تصویر', upload_to='media/%Y/%m/%d')
    caption = m.CharField(max_length=10, verbose_name='عنوان متای عکس')
    service = m.ForeignKey(Service, null=True, blank=True, verbose_name='خدمت مشمول عکس')

    class Meta:
        verbose_name = 'تصویر'
        verbose_name_plural = 'تصاویر'


class Comments(m.Model):
    user = m.ForeignKey(SepasUser)
    content = m.TextField(verbose_name='متن نظر')
    date = m.DateTimeField(auto_now_add=True, verbose_name='زمان ایجاد')
    service = m.ForeignKey(Service, null=True)


# class AddService_Restaurant(models.Model)
# ServiceProvider = models.ForeignKey(SepasUser, default='admin', verbose_name='تامین کننده ارائه دهنده')
#     RestaurantName = models.CharField(max_length=100, verbose_name='نام رستوران')
#     RestaurantCountry = models.CharField(max_length=100, verbose_name='گشور')
#     RestaurantCity = models.CharField(max_length=100, verbose_name='شهر')
#     RestaurantAddress = models.TextField(verbose_name='آدرس رستوران')
#     RestaurantRate = models.IntegerField(max_length=1, verbose_name='امتیاز رستوران')
#     RestaurantDescription = models.TextField(verbose_name='توضیحات')
#     CanReserve = models.BooleanField(verbose_name='قابلیت رزرو')
#     RestaurantMenu













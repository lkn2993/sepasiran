from django.shortcuts import render
from django.http import HttpRequest
from django.template import RequestContext
# Create your views here.
def error_404(request):
    """
    :return: error404
    """
    return render(request, '404.html', context_instance=RequestContext(request, {}))

def error_500(request):
    """
    :return: error500
    """
    return render(request, '500.html', context_instance=RequestContext(request, {}))
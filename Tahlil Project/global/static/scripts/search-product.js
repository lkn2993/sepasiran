/**
 * Created by tnt on 7/21/14.
 */
$(document).ready(function () {

    var products = new Bloodhound({
        datumTokenizer: function (d) {
            return Bloodhound.tokenizers.whitespace(d.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
//                remote: 'countries.json',
        prefetch: {
            url: '../static/scripts/json/products.json' // بایستی جیسونی باشد که جنگو پاس می دهد نه جیسون ثابت
        }
    });

    products.initialize();

    $('.typeahead').typeahead(null, {
        displayKey: 'value',
        source: products.ttAdapter(),
        templates: {
            empty: [
                '<div class="empty-message">',
                'شهری با نام وارد شده پیدا نشد.',
                '</div>'
            ].join('\n'),

            suggestion: Handlebars.compile(
//                '<div class="col-lg-12 pull-right">' +
                    '<div class="col-lg-10 pull-right"><h4>{{value}}</strong></h4></div>'
//                    '</div>'
            )
        }
    });




//    search product carousel slide
    $(document).ready(function () {

        $('.carousel').carousel();

    });
});
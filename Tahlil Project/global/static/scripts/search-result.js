/**
 * Created by maryam on 8/6/2015.
 */
$(document).ready(function () {

    $('#search-filter').click(function () {
        var from = $('#fromDate').attr("value");
        var to = $('#toDate').attr("value");
        var minPrice = $('#range_2').val().split(';')[0];
        var maxPrice = $('#range_2').val().split(';')[1];
        var originCountry = $('#originCountry option:selected').text();
        var originCity = $('#originCity option:selected').text();
        var destinationCountry = $('#destinationCountry option:selected').text();
        var destinationCity = $('#destinationCity option:selected').text();

        var data = {
            fromdate: from,
            todate: to,
            maxPrice: maxPrice,
            minPrice: minPrice,
            originCountry: originCountry,
            originCity: originCity,
            destinationCountry: destinationCountry,
            destinationCity: destinationCity
        };
        $.ajax(
            {
                cache: false,
                url: '.',
                type: 'post',
                data: data,
                dataType: "json",
                content_type: "application/json",
                error: errorHandler,
                success: success
            });
    });
    $('#search_form').submit(function (e) {
        e.preventDefault();
        var search_form = $('#search_form');
        ajaxPost(search_form.attr('action'), search_form.serialize(), function (content) {
            //onSuccess
            alert(content);
            var template = _.template($("#entry-template").text());
            var compiled = template(r);
            $('#existing').html(compiled);
        });
        return false;
    });
});
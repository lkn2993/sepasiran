"""
Definition of urls for Tahlil_Project.
"""

from datetime import datetime
from django.conf.urls import patterns, url, include
from django.views.generic import RedirectView
from django.contrib import admin
from .settings import MEDIA_ROOT, STATIC_ROOT
admin.autodiscover()


handler404 = 'global.views.error_404'
handler500 = 'global.views.error_500'

urlpatterns = patterns('',
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^home/', include('app.urls', namespace="app")),
    url(r'^$', RedirectView.as_view(url='home/', permanent = True)),
    url(r'^profile/', include('Profile.urls', namespace="Profile")),
    url(r'^dashboard/', include('Manager_Dashboard.urls', namespace="Manager_Dashboard")),
    url(r'^Co_Panel/', include('Co_Panel.urls', namespace="Co_Panel")),
    url(r'^about', 'app.views.about', name='about'),
    url(r'^login/$', 'Profile.views.login', name='login'),
    url(r'^logout/$',
        'django.contrib.auth.views.logout',
        {
            'next_page': '/',
        },
        name='logout'),
    url(r'^media/(?P<path>.*)$',
            'django.views.static.serve',
            {'document_root': MEDIA_ROOT, }),
    url(r'^static/(?P<path>.*)$',
            'django.views.static.serve',
            {'document_root': STATIC_ROOT, }),
    url(r'^payment/', include('payment.urls', namespace='payment')),
)




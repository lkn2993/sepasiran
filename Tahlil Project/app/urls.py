from django.conf.urls import url

urlpatterns = [
    url(r'^$', 'app.views.home', name='home'),
    url(r'^tour-details/(?P<id>[0-9]+)/$', 'app.views.tour_details', name='tour_details'),
    url(r'^search-result/$', 'app.views.search_result', name='search_result'),
    # url(r'^search-result/filter$', 'app.views.search_filter', name='search_filter')
    url(r'^contactus/next_url=(?P<next_url>.*)$', 'app.context_processors.contact_form_process', name='contact_form_process'),
    url(r'^search-result/search/$', 'app.views.search_detailed', name='search_detailed'),
]
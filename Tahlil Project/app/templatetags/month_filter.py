from django import template
register = template.Library()

@register.filter
def month_filter(value):
    month = [
        'فروردین',
        'اردیبهشت',
        'خرداد',
        'تیر',
        'مرداد',
        'شهریور',
        'مهر',
        'آبان',
        'آذر',
        'دی',
        'بهمن',
        'اسفند'
    ]
    value = month[value-1]
    return value

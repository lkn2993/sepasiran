﻿# -*- coding: utf-8 -*-
"""
Definition of views.
"""
import json
from django.views.generic import TemplateView
from django.views.generic import FormView
from django.shortcuts import render
from django.http import HttpRequest, HttpResponse, JsonResponse
from django.template import RequestContext, Context
from datetime import datetime
from khayyam3 import JalaliDate
from .forms import NameForm, ContactForm
from toursNservices.forms import SearchForm
from toursNservices.models import Tour
from Profile.models import SepasUser
from django.core.mail import send_mail
from django_ajax.decorators import ajax
from CPRM.models import Report
# views by ali esmaeili      start

def home(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    if not SepasUser.objects.filter(is_admin=True).first():
        SepasUser.objects.create_superuser('ali', 'kiaian', 'lkn2993@gmail.com', '1999-11-11', 'aliali')
    if not Report.objects.first():
        Report.objects.create()
    r = Report.objects.get(pk=1)
    r.home_page_viewed += 1
    r.save()
    search_form = SearchForm()

    lasttours = Tour.objects.all().order_by('-created_date')[:4]
    most_visited = Tour.objects.filter(views__gte=1).order_by('-views')[:6]
    return render(
        request,
        'app/index.html',
        context_instance=RequestContext(request, {'title': 'خانه', 'form': ContactForm(), 'search_form': search_form,
                                                  'lasttours': lasttours, 'most_visited': most_visited})
    )


def tour_details(request, id):
    """
    :param request: clicked on tour thumbnail
    :return: tour details page
    """
    assert isinstance(request, HttpRequest)
    tour = Tour.objects.get(pk=id)
    tour.viewed += 1
    tour.save()
    # categury = Tour.objects.get(pk=id).categury.all()
    # for cat in categury:
    # cat.name
    # similar_tours = Tour.objects.filter(categury=categury)[:4]
    return render(request, 'app/tourdetails.html',
                  context_instance=RequestContext(request, {'title': 'تور...', 'tour': tour}))


def search_result(request):
    """
    :param request: clicked on search
    :return: search_result page
    """
    if request.is_ajax() and request.method == 'POST':
        departure = request.POST.get('fromdate', None)
        arrival = request.POST.get('todate', None)
        min_price = request.POST.get('minPrice', None)
        max_price = request.POST.get('maxPrice', None)
        origin_country = request.POST.get('originCountry', None)
        origin_city = request.POST.get('originCity', None)
        destination_country = request.POST.get('destinationCountry', None)
        destination_city = request.POST.get('destinationCity', None)
        exist_tours = Tour.objects.filter(departure__gte=departure, arrival__lte=arrival, price__gte=min_price,
                                          price__lte=max_price, origin_country=origin_country, origin_city=origin_city,
                                          destination_city=destination_city, destination_country=destination_country)

        # JalaliDate(t.departure.day).strftime('%A')
        tours = []
        for tour in exist_tours:
            t = {}
            t['name'] = tour.name
            t['price'] = tour.price
            t['image'] = tour.images.image.url
            t['description'] = tour.description
            t['duration'] = tour.duration
            t['destination_city'] = tour.destination_city
            t['departure_day'] = tour.departure.day
            t['departure_month'] = JalaliDate.from_date(tour.departure).strftime('%B')
            t['arrival_day'] = tour.arrival.day
            t['arrival_month'] = JalaliDate.from_date(tour.arrival).strftime('%B')
            t['transport_type'] = tour.get_transport_type_display()
            # print(tour.get_transport_type_display())
            tours.append(t)

        result = {
            'tour': tours
        }
        return HttpResponse(
            json.dumps(result),
            content_type="application/json"
        )
    #TODO: change these fields to match overhaul
    #if request.method == 'POST':
    #    search_form = SearchForm(request.POST)
    #    if search_form.is_valid():
    #        origin_city = search_form.cleaned_data['origin_city']
    #        destination_city = search_form.cleaned_data['destination_city']
    #        departure = search_form.cleaned_data['departure']
    #        arrival = search_form.cleaned_data['arrival']
    #        searched_tours = Tour.objects.filter(origin_city=origin_city, destination_city=destination_city,
    #                                             departure__gte=departure, arrival__lte=arrival)
    #        x = searched_tours
    #        return render(request, 'app/search_result.html', {'searched_tours': searched_tours})
    assert isinstance(request, HttpRequest)
    return render(request, 'app/search_result.html', context_instance=RequestContext(request, {'title': 'نتایج جستجو'}))


# views by ali esmaeili      end
@ajax
def search_detailed(request):
    a = 1 + 1
    return JsonResponse({'a' : a})

def about(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/about.html',
        context_instance=RequestContext(request,
                                        {
                                            'title': 'درباره ی سپاس ایران',
                                        })
    )
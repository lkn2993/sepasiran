﻿"""
Definition of forms.
"""

from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.utils.translation import ugettext_lazy as _
from django.core.mail import send_mail
from django.core.files.uploadedfile import SimpleUploadedFile


class NameForm(forms.Form):
    your_name = forms.CharField(label='Your Name', max_length=20)


class ContactForm(forms.Form):
    subject = forms.CharField(max_length=100,widget=forms.TextInput({'class':'input-sm form-control', 'placeholder':'موضوع پیام چیست؟'}))
    message = forms.CharField(widget=forms.Textarea({'class':'input-sm form-control', 'cols':'5', 'rows':'5'}))
    sender = forms.EmailField(widget=forms.TextInput({'class':'input-sm form-control', 'placeholder':'لطفا ایمیل خود را وارد کنید'}))
    cc_myself = forms.BooleanField(required=False)

    def send_email(self):
        # send email using the self.cleaned_data dictionary
        subject = self.cleaned_data['subject']
        message = self.cleaned_data['message']
        sender = self.cleaned_data['sender']
        cc_myself = self.cleaned_data['cc_myself']
        recipients = ['a.esmaeili.sut@gmail.com']
        if cc_myself:
            recipients.append(sender)
        send_mail(subject, message, sender, recipients)


from django import forms
from .forms import ContactForm
from django.core.urlresolvers import reverse
def contact_form(request):
    return {
        'contact_form': ContactForm(),
        'contact_form_url': reverse("app:contact_form_process", kwargs=({'next_url' : request.path}))
    }

def contact_form_process(request, next_url):
        # if the top login form has been posted
        if request.method == 'POST' and 'subject' in request.POST and 'message' in request.POST and 'sender' in request.POST:
            contact_form = ContactForm(request.POST)
            if form.is_valid():
                subject = contact_form.cleaned_data['subject']
                message = contact_form.cleaned_data['message']
                sender = contact_form.cleaned_data['sender']
                cc_myself = contact_form.cleaned_data['cc_myself']
                recipients = ['a.esmaeili.sut@gmail.com']
                if cc_myself:
                    recipients.append(sender)

                send_mail(subject, message, sender, recipients)
        else:
            contact_form = ContactForm()
        return redirect(next_url)
